// Imports
import Vue from "vue";
import VueRouter from "vue-router";
import axios from "axios";
import VueAxios from "vue-axios";
import App from "./App.vue";
import { BootstrapVue, IconsPlugin } from "bootstrap-vue";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";
import GameMenu from "./components/GameMenu.vue";
import GamePlay from "./containers/GamePlay.vue";
import GameOver from "./components/GameOver.vue";

Vue.use(VueRouter);
Vue.use(BootstrapVue);
Vue.use(IconsPlugin);
Vue.use(VueAxios, axios);

Vue.config.productionTip = false;
// Using Axios for the fetch of API
Vue.prototype.$axios = axios;

/**
 * Creating VueRouter and it's routes
 */
const router = new VueRouter({
  routes: [
    { path: "/", name: "GameMenu", component: GameMenu },
    { path: "/play", name: "GamePlay", component: GamePlay },
    { path: "/game-over", name: "GameOver", component: GameOver }
  ]
});

new Vue({
  router,
  render: h => h(App)
}).$mount("#app");
