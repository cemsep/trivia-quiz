/**
 * Player Class
 */
export default class Player {
  constructor(name, playerAnswers, score) {
    this.name = name;
    this.playerAnswers = playerAnswers;
    this.score = score;
  }

  getName() {
    return this.name;
  }

  getAnswers() {
    return this.playerAnswers;
  }

  getScore() {
    return this.score;
  }

  addAnswer(answer) {
    this.playerAnswers.push(answer);
  }

  addScore() {
    this.score = this.score + 10;
  }
}
