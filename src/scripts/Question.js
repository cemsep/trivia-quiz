// Importing jquery for the decoding of the html quotes
import $ from "jquery";

/**
 * Question class
 */
export default class Question {
  constructor(question) {
    this.category = question.category;
    // Decoding html quotes
    this.questionText = $("<textarea/>")
      .html(question.question)
      .text();
    // Decoding html quotes
    this.correctAnswer = $("<textarea/>")
      .html(question.correct_answer)
      .text();
    // Decoding html quotes
    this.incorrectAnswers = this.getIncorrectAnswers(
      question.incorrect_answers
    );
  }

  getCategory() {
    return this.category;
  }

  getQuestionText() {
    return this.questionText;
  }

  getCorrectAnswer() {
    return this.correctAnswer;
  }

  getIncorrectAnswers(incorrectAnswers) {
    // Decoding html quote before returning
    incorrectAnswers = incorrectAnswers.map(answer => {
      return $("<textarea/>")
        .html(answer)
        .text();
    });
    return incorrectAnswers;
  }

  get possibleAnswers() {
    if (this.incorrectAnswers.length === 1) {
      return ["True", "False"];
    }
    let answers = this.incorrectAnswers.concat(this.correctAnswer);
    for (var i = answers.length - 1; i > 0; i--) {
      let j = Math.floor(Math.random() * (i + 1));
      [answers[i], answers[j]] = [answers[j], answers[i]];
    }
    return answers;
  }
}
