FROM node:lts-alpine

ENV PORT 3000

WORKDIR /src
COPY package.json .
RUN npm install    
COPY . .

RUN npm run build

EXPOSE 3000/tcp

CMD [ "npm", "start" ]