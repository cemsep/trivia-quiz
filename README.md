# trivia-quiz

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Starts express app
```
npm run start
```

### Lints and fixes files
```
npm run lint
```

## Components

### GameMenu

A start page where the player can register his/her name and click a "Start Game" button to start the game.


### GamePlay

A component that handles the active game. It displays the QuizQuestion component. The GamePlay contains the data for questions and scores.


### QuizQuestion

A simple component that display the question with the available options (True/False) or multiple choice. It receives the question as a prop. The component emits a custom event when the answer is selected.


### GameOver

A component that displays the result of the current game. It also shows the QuizResults component. Other thing that it contains is buttons for restart the game and go back to the GameMenu component.


### QuizResults

The component receives data set to display the answered questions.


### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
