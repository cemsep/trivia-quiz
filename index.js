"use strict";

/**
 * Dependencies
 * @ignore
 */
const path = require("path");
const express = require("express");
const morgan = require("morgan");
//const history = require("connect-history-api-fallback");

/**
 * Dotenv
 * @ignore
 */
require("dotenv").config();

/**
 * Express
 * @ignore
 */
const { PORT: port = 3000 } = process.env;
const app = express();

app.use(morgan("tiny"));
app.use(express.static(path.join("dist")));

/**
 * Launch app
 * @ignore
 */
app.listen(port, () => console.log(`Example app listening on port ${port}!`));
